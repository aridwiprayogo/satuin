-- phpMyAdmin SQL Dump
-- version 4.7.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 21, 2017 at 11:06 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `satuin`
--

-- --------------------------------------------------------

--
-- Table structure for table `data diri`
--

CREATE TABLE `data diri` (
  `id data diri` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `nomor telepon` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `tanggal lahir` date NOT NULL,
  `tempat lahir` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `agama` varchar(10) NOT NULL,
  `status` varchar(20) NOT NULL,
  `foto profil` varchar(100) NOT NULL
  `idketertarikan` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  `agama` varchar(10) NOT NULL,
  `tinggi badan` int(3) NOT NULL,
  `nama sekolah` varchar(50) NOT NULL
  `suku` varchar(50) NOT NULL,
  `pekerjaan` varchar(20) NOT NULL,
  `nama perusahaan` varchar(50) NOT NULL,
  `penghasilan` int(9) NOT NULL,
  `genre musik` varchar(10) NOT NULL,
  `artist favorit` varchar(50) NOT NULL,
  `lagu favorit` varchar(20) NOT NULL,
  `genre film` varchar(20) NOT NULL,
  `judul film favorit` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1

-- --------------------------------------------------------

--
-- Table structure for table `pekerjaan`
--

CREATE TABLE `pekerjaan` (
  `id pekerjaan` varchar(20) NOT NULL,
  `jabatan` int(20) NOT NULL,
  `lama bekerja` time(6) NOT NULL,
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan`
--

CREATE TABLE `pendidikan` (
  `id pendidikan` varchar(20) NOT NULL,
  `jenjang` varchar(20) NOT NULL,
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE `register` (
  `id register` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data diri`
--
ALTER TABLE `data diri`
  ADD PRIMARY KEY (`id data diri`);

--
-- Indexes for table `pekerjaan`
--
ALTER TABLE `pekerjaan`
  ADD PRIMARY KEY (`id pekerjaan`);

--
-- Indexes for table `pendidikan`
--
ALTER TABLE `pendidikan`
  ADD PRIMARY KEY (`id pendidikan`);

--
-- Indexes for table `register`
--
ALTER TABLE `register`
  ADD PRIMARY KEY (`id register`) ;
--
-- Indexes for table `ketertarikan`
--

ALTER TABLE `ketertarikan`
  ADD PRIMARY KEY(`idketertarikan`);
--
-- Constraints for dumped tables
--

--
-- Constraints for table `data diri`
--
ALTER TABLE `data diri`
  ADD CONSTRAINT `data diri_ibfk_1` FOREIGN KEY (`id data diri`) REFERENCES `pekerjaan` (`id pekerjaan`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `register`
--
ALTER TABLE `register`
  ADD CONSTRAINT `register_ibfk_1` FOREIGN KEY (`id register`) REFERENCES `data diri` (`id data diri`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
